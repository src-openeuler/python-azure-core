%global _empty_manifest_terminate_build 0
Name:           python-azure-core
Version:        1.32.0
Release:        1
Summary:        Microsoft Azure Core Library for Python
License:        MIT
URL:            https://github.com/Azure/azure-sdk-for-python/tree/master/sdk/core/azure-core
Source0:        https://files.pythonhosted.org/packages/cc/ee/668328306a9e963a5ad9f152cd98c7adad86c822729fd1d2a01613ad1e67/azure_core-1.32.0.tar.gz
BuildArch:      noarch
%description
 Azure Core shared client library for PythonAzure core provides shared
exceptions and modules for Python SDK client libraries.

%package -n python3-azure-core
Summary:        Microsoft Azure Core Library for Python
Provides:       python-azure-core
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
BuildRequires:  python3-six
BuildRequires:  python3-requests
# General requires
Requires:       python3-six
Requires:       python3-requests
%description -n python3-azure-core
 Azure Core shared client library for PythonAzure core provides shared
exceptions and modules for Python SDK client libraries.

%package help
Summary:        Microsoft Azure Core Library for Python
Provides:       python3-azure-core-doc
%description help
 Azure Core shared client library for PythonAzure core provides shared
exceptions and modules for Python SDK client libraries.

%prep
%autosetup -n azure_core-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-azure-core -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Nov 7 2024 guochao <guochao@kylinos.cn> - 1.32.0-1
- Update package to version 1.32.0
- Added a default implementation to handle token challenges in BearerTokenCredentialPolicy and 
  AsyncBearerTokenCredentialPolicy.
- Fixed an issue where the tracing_attributes keyword argument wasn't being handled at the 
  request/method level


* Thu Oct 24 2024 liutao1 <liutao1@kylinos.cn> - 1.31.0-1
- Update package to version 1.31.0

* Wed Jul 24 2024 liudy <liudingyao@kylinos.cn> - 1.30.2-1
- Update package to version 1.30.2
- Tracing: DistributedTracingPolicy will now set an attribute, http.request.resend_count, 
   on HTTP spans for resent requests to indicate the resend attempt number

* Tue Apr 02 2024 wangqiang <wangqiang1@kylinos.cn> - 1.30.1-1
- Update package to version 1.30.1

* Mon Jul 25 2022 renliang16 <renliang@uniontech.com> - 1.6.0-1
- Init package python3-azure-core of version 1.6.0

